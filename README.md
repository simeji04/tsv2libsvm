# tsv2libsvm

tsvとlibsvm形式の相互変換ツール(自分用)



***DEMO:***


## Features

- tsv形式のデータからlibsvm形式のデータに変換
- libsvm形式のデータからtsv形式のデータに変換



## Installation

    $ git clone https://github.com/simeji04/tsv2libsvm


## Author
simeji04
